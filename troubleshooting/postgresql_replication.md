# Postgresql replication is lagging or has stopped

## First and foremost

*Don't Panic*

## Symptoms

* Alert that replication is lagging behind

## Possible checks

* Monitoring

## Resolution

Just wait, replication self recovers
